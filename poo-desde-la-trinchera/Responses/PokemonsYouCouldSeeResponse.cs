﻿using DLT_Service.Remote.RickAndMorty.Contracts;

namespace poo_desde_la_trinchera.Responses;

public class PokemonsYouCouldSeeResponse
{
    public PokemonsYouCouldSeeResponse()
    {
        this.Pokemons = new List<PokemonDto>();
    }

    public string Name { get; internal set; }
    public LocationResponse Location { get; internal set; }
    public string Image { get; internal set; }
    public List<PokemonDto> Pokemons { get; internal set; }
}