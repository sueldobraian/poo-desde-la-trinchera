﻿namespace poo_desde_la_trinchera.Responses;

public class LocationResponse
{
    public LocationResponse(string id, string name)
    {
        this.Id = id;
        this.Name = name;
    }

    public string Id { get; internal set; }
    public string Name { get; internal set; }
}
