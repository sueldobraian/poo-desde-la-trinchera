﻿using DLT_Service.Remote.RickAndMorty.Services;
using Refit;

namespace poo_desde_la_trinchera;

public static class ApplicationStartup
{
    public static void AddHttpClientFactory(this IServiceCollection services)
    {
        services.AddRefitClient<IRickAndMortyClient>()
                .ConfigureHttpClient((serviceProvider, client) =>
                {
                    client.BaseAddress = new Uri("https://rickandmortyapi.com/api");
                }); 

        services.AddRefitClient<IPokemonClient>()
                .ConfigureHttpClient((serviceProvider, client) =>
                {
                    client.BaseAddress = new Uri("https://pokeapi.co/api/v2");
                });
    }
}
