using DLT_Service.Remote.RickAndMorty.Contracts;
using DLT_Service.Remote.RickAndMorty.Services;
using Microsoft.AspNetCore.Mvc;
using poo_desde_la_trinchera.Responses;

namespace poo_desde_la_trinchera.Controllers;

[ApiController]
[Route("[controller]")]
public class RickAndMortyController : ControllerBase
{
    private readonly IRickAndMortyClient rickAndMortyClient;
    private readonly IPokemonClient pokemonClient;

    public RickAndMortyController(IRickAndMortyClient rickAndMortyClient, IPokemonClient pokemonClient)
    {
        this.rickAndMortyClient = rickAndMortyClient;
        this.pokemonClient = pokemonClient;
    }

    [HttpGet("PokemonsYouCouldSeeResponse")]
    [Consumes("application/json")]
    [Produces("application/json")]
    [ProducesDefaultResponseType]
    public async Task<IActionResult> PokemonsYouCouldSee([FromQuery] string? name)
    {
        var response = new PokemonsYouCouldSeeResponse();

        var character = (await this.rickAndMortyClient.GetCharacterAsync(name))?.Results.FirstOrDefault();

        if (character == null)
        { return new NotFoundResult(); }

        response.Name = character.Name;
        response.Image = character.Image;
        response.Location = new (character.Location.Url.Split('/').Last(), character.Location.Name);
        var residents = (await this.rickAndMortyClient.GetLocationAsync(response.Location.Id))?.Residents.Select(l => l.Split('/').Last());
        
        if (residents.Any())
        {
            var tasks = new List<Task<PokemonDto>>();
            foreach (var r in residents)
            {  tasks.Add(this.pokemonClient.GetAsync(r)); }

            Task.WhenAll(tasks);

            tasks.ForEach(t => response.Pokemons.Add(t.Result));
        }

        return Ok(response);
    }
}
