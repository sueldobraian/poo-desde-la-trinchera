﻿using DLT_Service.Remote.RickAndMorty.Contracts;
using Refit;

namespace DLT_Service.Remote.RickAndMorty.Services
{
    public interface IPokemonClient
    {
        [Get("/pokemon/{id}")]
        Task<PokemonDto> GetAsync(string id);
    }
}
