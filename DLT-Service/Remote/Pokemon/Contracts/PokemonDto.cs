﻿using System.Text.Json.Serialization;

namespace DLT_Service.Remote.RickAndMorty.Contracts;

public class PokemonDto
{
    public List<Ability> Abilities { get; set; }
    public int Base_experience { get; set; }
    public List<Form> Forms { get; set; }
    public bool Is_default { get; set; }
    public List<MoveDto> Moves { get; set; }
    public string Name { get; set; }
    public int Order { get; set; }
    public Sprites Sprites { get; set; }
    public int Weight { get; set; }
}

public class Ability
{
    //  public Ability Ability { get; set; }
    public bool Is_hidden { get; set; }
    public int Slot { get; set; }
}

public class Form
{
    public string Name { get; set; }
    public string Url { get; set; }
}

public class MoveDto
{
    public Move Move { get; set; }
}

public class Move
{
    public string Name { get; set; }
    public string Url { get; set; }
}

public class Sprites
{
    [JsonPropertyName("back_default")]
    public string BackDefault { get; set; }
    [JsonPropertyName("back_female")]
    public object BackFemale { get; set; }
    [JsonPropertyName("back_shiny")]
    public string BackShiny { get; set; }
    [JsonPropertyName("back_shiny_female")]
    public object BackShinyFemale { get; set; }
    [JsonPropertyName("front_default")]
    public string FrontDefault { get; set; }
    [JsonPropertyName("front_female")]
    public object FrontFemale { get; set; }
    [JsonPropertyName("front_shiny")]
    public string FrontShiny { get; set; }
    [JsonPropertyName("front_shiny_female")]
    public object FrontShinyFemale { get; set; }
}
