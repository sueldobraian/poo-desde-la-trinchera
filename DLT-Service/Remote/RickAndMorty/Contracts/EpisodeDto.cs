﻿namespace DLT_Service.Remote.RickAndMorty.Contracts;

public class EpisodeDto
{
    public EpisodeInfo Info { get; set; }
    public List<EpisodeResult> Results { get; set; }
}

public class EpisodeInfo
{
    public int Count { get; set; }
    public int Pages { get; set; }
    public string Next { get; set; }
    public object Prev { get; set; }
}

public class EpisodeLocation
{
    public string Name { get; set; }
    public string Url { get; set; }
}

public class EpisodeOrigin
{
    public string Name { get; set; }
    public string Url { get; set; }
}

public class EpisodeResult
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Status { get; set; }
    public string Species { get; set; }
    public string Type { get; set; }
    public string Gender { get; set; }
    public EpisodeOrigin Origin { get; set; }
    public EpisodeLocation Location { get; set; }
    public string Image { get; set; }
    public List<string> Episode { get; set; }
    public string Url { get; set; }
    public DateTime Created { get; set; }
}
