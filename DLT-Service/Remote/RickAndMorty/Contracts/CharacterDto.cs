﻿namespace DLT_Service.Remote.RickAndMorty.Contracts;

public class CharacterDto
{
    public CharacterInfo Info { get; set; }
    public List<CharacterResult> Results { get; set; }
}

public class CharacterInfo
{
    public int Count { get; set; }
    public int Pages { get; set; }
    public string Next { get; set; }
    public object Prev { get; set; }
}

public class CharacterLocation
{
    public string Name { get; set; }
    public string Url { get; set; }
}

public class CharacterOrigin
{
    public string Name { get; set; }
    public string Url { get; set; }
}

public class CharacterResult
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Status { get; set; }
    public string Species { get; set; }
    public string Type { get; set; }
    public string Gender { get; set; }
    public CharacterOrigin Origin { get; set; }
    public CharacterLocation Location { get; set; }
    public string Image { get; set; }
    public List<string> Episode { get; set; }
    public string Url { get; set; }
    public DateTime Created { get; set; }
}
