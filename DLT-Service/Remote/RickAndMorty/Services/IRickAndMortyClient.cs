﻿using DLT_Service.Remote.RickAndMorty.Contracts;
using Refit;

namespace DLT_Service.Remote.RickAndMorty.Services;

public interface IRickAndMortyClient
{
    [Get("/character/?name={name}")]
    Task<CharacterDto> GetCharacterAsync(string name);

    [Get("/episode/{ids}")]
    Task<IEnumerable<EpisodeDto>> GetEpisodeAsync(string ids);

    [Get("/location/{ids}")]
    Task<LocationDto> GetLocationAsync(string ids);
}
